import pandas as pd
import numpy as np
import sys
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
canvas1 = tk.Canvas(root, width=300, height=300, bg='lightsteelblue2', relief='raised') #, width=300, height=300, bg='lightsteelblue2', relief='raised'
canvas1.pack() #side=tk.BOTTOM

def readExcel():
    import_file = filedialog.askopenfilename()

    data = pd.read_excel(import_file, engine='openpyxl')
    data['Transaction Date'] = pd.to_datetime(data['Transaction Date']).dt.date
    data['Settlement Date'] = pd.to_datetime(data['Settlement Date']).dt.date
    return data

def exportExcel():
    #global data
    data = readExcel()
    export_file_path = filedialog.asksaveasfilename(defaultextension='.xlsx')
    data.to_excel(export_file_path, index=False, header=True)
    
saveAsButtonExcel = tk.Button(canvas1, text='Select and Export Excel', command=exportExcel, bg='green', fg='blue', font=('helvetica', 12, 'bold'))
saveAsButtonExcel.pack( side =tk.BOTTOM)
saveAsButtonExcel.configure(width = 0, activebackground = "#D2D2D2", relief ='raised')

root.mainloop()

# run using $ python3 ./projectFolder/converter.py