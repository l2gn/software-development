This software allows you to convert Paystack datetime formatted columns into usable date only columns.

Steps to follow:

1. Clone this repository or download the repository to a safe environment.
2. Open a terminal or shell instance as Admin for use in installing the package to wrap this code as a software.
3. Run the following code in your terminal or shell environment:
        
        For MacOS or Linux
            $ sudo pip3 install pyinstaller  **Note: running codes in sudo needs elevated access.**
        
        For Windows
            C:\\Users\Vangelis> pip install pyinstaller

4. Give sometime to allow the above code install. Once done, locate the folder with the converter.py file and use the following:
            
            $ pyinstaller --onefile -w 'converter.py'

5. The converter can sometimes fail, run the following if the above fails:
            
            $ .\pyinstaller --onefile -w 'converter.py'

6. If you encounter dependency issues (missing libraries and packages), use the following:
            
            $ pyinstaller --hidden-import 'package_name' --onefile 'converter.py'

7. Give quite sometime and once done, head over to the folder with the 'converter.py' file in it. You should see a new folder with the title 'dist'
    Within this folder, you should see your 'converter.exe' application. Use this to load up your application.
    
*CHEERS*

